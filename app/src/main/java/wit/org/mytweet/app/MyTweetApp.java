package wit.org.mytweet.app;

import wit.org.mytweet.models.Portfolio;
import wit.org.mytweet.models.PortfolioSerializer;

import wit.org.mytweet.app.MyTweetServiceProxy;
import wit.org.mytweet.models.Tweet;
import wit.org.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;
import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

import static wit.org.android.helpers.LogHelpers.info;

public class MyTweetApp extends Application
{
    private static final String FILENAME = "portfolio.json";

    public Portfolio portfolio;
    //public String          service_url  = "http://10.0.2.2:9000";    //Android Emulator
    //public String          service_url  = "http://10.0.3.2:9000";    //Genymotion
    public String          service_url  = "https://mytweetservicewebapp.herokuapp.com/";

    public MyTweetServiceProxy tweetService;
    public boolean              tweetServiceAvailable = false;
    public List<Tweet> tweets = new ArrayList<Tweet>();
    public List<Tweeter> tweeters = new ArrayList<Tweeter>();
    public Tweeter loggedOnTweeter;

    @Override
    public void onCreate()
    {
        super.onCreate();
        PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(serializer);
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        tweetService = retrofit.create(MyTweetServiceProxy.class);

        info(this, "MyTweet app launched");
    }

    public void newTweeter(Tweeter tweeter)
    {
        tweeters.add(tweeter);
    }

    public void newTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public boolean validTweeter (String email, String password)
    {
        for (Tweeter tweeter : tweeters)
        {
            if (tweeter.email.equals(email) && tweeter.password.equals(password))
            {
                loggedOnTweeter = tweeter;
                return true;
            }
        }
        return false;
    }
}
