package wit.org.mytweet.models;

import java.util.UUID;

public class Tweeter
{
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;


    public Tweeter(String firstName, String lastName, String email, String password)
    {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }
}
