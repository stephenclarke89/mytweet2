package wit.org.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import wit.org.mytweet.R;
import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Tweeter;

public class SignupActivity extends AppCompatActivity implements Callback<Tweeter>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
    }

    public void registerPressed (View view)
    {
        TextView firstName = (TextView) findViewById(R.id.sfirstName);
        TextView lastName  = (TextView) findViewById(R.id.slastName);
        TextView email     = (TextView) findViewById(R.id.sEmail);
        TextView password  = (TextView) findViewById(R.id.sPassword);

        Tweeter tweeter = new Tweeter (firstName.getText().toString(), lastName.getText().toString(),
                email.getText().toString(), password.getText().toString());

        MyTweetApp app = (MyTweetApp) getApplication();
        Call<Tweeter> call = (Call<Tweeter>) app.tweetService.createTweeter(tweeter);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<Tweeter> response, Retrofit retrofit)
    {
        app.tweeters.add(response.body());
        startActivity(new Intent(this, WelcomeActivity.class));
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        Toast toast = Toast.makeText(this, "Service Unavailable. Please Try again", Toast.LENGTH_LONG);
        toast.show();
        startActivity (new Intent(this, WelcomeActivity.class));
    }
}
