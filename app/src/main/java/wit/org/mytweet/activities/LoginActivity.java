package wit.org.mytweet.activities;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import wit.org.mytweet.R;
import wit.org.mytweet.app.MyTweetApp;

public class LoginActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void loginPressed (View view)
    {
        MyTweetApp app = (MyTweetApp) getApplication();

        TextView email     = (TextView)  findViewById(R.id.lEmail);
        TextView password  = (TextView)  findViewById(R.id.lPassword);

        if (app.validTweeter(email.getText().toString(), password.getText().toString()))
        {
            startActivity (new Intent(this, TweetListActivity.class));
        }
        else
        {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
