package wit.org.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import wit.org.mytweet.R;

public class WelcomeActivity extends AppCompatActivity implements Callback<List<Tweeter>>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        app = (MyTweetApp) getApplication();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //app.loggedOnTweeter = null;
        Call<List<Tweeter>> call =(Call<List<Tweeter>>) app.tweetService.getAllTweeters();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit)
    {
        serviceAvailableMessage();
        app.tweeters = response.body();
        app.tweetServiceAvailable = true;
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        serviceUnavailableMessage();
        //Toast.makeText(this, "Failed to retrieve tweeter list", Toast.LENGTH_LONG).show();
    }

    public void signupPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity(new Intent(this, SignupActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void loginPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity(new Intent(this, LoginActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "Service Unavailable. Please  try again", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        int numberTweeters = app.tweeters.size();
        Toast.makeText(this, "Retrieved " + numberTweeters + " tweeters", Toast.LENGTH_LONG).show();
    }
}
